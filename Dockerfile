FROM debian:stretch
LABEL maintainer="ProfessorManhattan"

ENV container docker
ENV DEBIAN_FRONTEND noninteractive

# Source: https://github.com/geerlingguy/docker-debian9-ansible/blob/master/Dockerfile
# Source: https://github.com/j8r/dockerfiles/blob/master/systemd/debian/9.Dockerfile

WORKDIR /

COPY initctl .

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]
# hadolint ignore=DL3003,SC2010
RUN set -xe \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        python3=3.* \
        python3-apt=1.* \
        sudo=1.* \
        systemd=* \
        systemd-sysv=* \
    && rm -Rf /usr/share/doc \
        /usr/share/man \
        /var/tmp/* \
        /tmp/* \
    && apt-get clean \
    && chmod +x initctl \
    && rm -rf /sbin/initctl \
    && ln -s /initctl /sbin/initctl \
    && mkdir -p /etc/ansible \
    && printf "[local]\nlocalhost ansible_connection=local" > /etc/ansible/hosts \
    && groupadd -r ansible \
    && useradd -m ansible -p ansible -g ansible \
    && usermod -aG sudo ansible \
    && sed -i "/^%sudo/s/ALL\$/NOPASSWD:ALL/g" /etc/sudoers \
    && rm -f /lib/systemd/system/multi-user.target.wants/* \
        /etc/systemd/system/*.wants/* \
        /lib/systemd/system/local-fs.target.wants/* \
        /lib/systemd/system/sockets.target.wants/*udev* \
        /lib/systemd/system/sockets.target.wants/*initctl* \
        /lib/systemd/system/sysinit.target.wants/systemd-tmpfiles-setup* \
        /lib/systemd/system/systemd-update-utmp*

VOLUME ["/sys/fs/cgroup", "/tmp", "/run"]

CMD ["/lib/systemd/systemd"]
